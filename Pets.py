class Pets:
    def __init__(self, id, gatunek, kolor, cena):
        self.id = id
        self.gatunek = gatunek
        self.kolor = kolor
        self.cena = cena

    def __str__(self):
        return self.id + " " + self.gatunek + " " + self.kolor + " " + str(self.cena)