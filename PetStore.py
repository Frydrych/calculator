import uuid

from Pets import Pets

balance = 1000
outputlist = []

def menu():
    print("""====================PET STORE==============================

=================MENU===========================
1. Dodaj zwierzaka
2. Sprzedaj zwierzaka
3. Lista zwierzków na sprzedaż
4. Wyszukiwarka
================================================""")
    choise = int(input())
    if (choise == 1):
        add()
    if (choise == 2):
        sell()
    if (choise == 3):
        print("\n============LISTA ZWIERZAT============")
        for pet in outputlist:
            print(pet)
        menu()
    if (choise == 4):
        search()


def add():
    global outputlist
    id = str(uuid.uuid4())
    gatunek = input("Gatunek:")
    kolor = input("Kolor:")
    cena = int(input("Cena:"))
    pet = Pets(id, gatunek, kolor, cena)
    outputlist.append(pet)
    menu()

def sell():
    global outputlist
    global balance
    for pet in outputlist:
        print(pet)
    out = input("Wybierz z listy zwierze do sprzedazy:")
    discount = input("Podaj zniżkę:")
    for pet in outputlist:
        if out == pet.id:
            outputlist.remove(pet)
            balance += pet.cena - (pet.cena * (int(discount) / 100))
            print("Obecny bilans firmy:" + str(balance))
    for pet in outputlist:
        print(pet)



    menu()


def search_pet_by_id():
    id = input("Podaj uuid").strip()
    list_of_pets_with_specified_uuid = []

    for pet in outputlist:
        if pet.id == id:
            list_of_pets_with_specified_uuid.append(pet)
    for pet in list_of_pets_with_specified_uuid:
        print(pet)
    menu()


def search_pet_by_species():
    specify = input("Podaj gatunek").strip()
    list_of_pets_with_specified_specify = []

    for pet in outputlist:
        if pet.gatunek == specify:
            list_of_pets_with_specified_specify.append(pet)
    for pet in list_of_pets_with_specified_specify:
        print(pet)
    menu()


def search_pet_by_color():
    color = input("Podaj kolor").strip()
    list_of_pets_with_specified_color = []

    for pet in outputlist:
        if pet.kolor == color:
            list_of_pets_with_specified_color.append(pet)
    for pet in list_of_pets_with_specified_color:
        print(pet)
    menu()


def search_pet_by_price():
    price = int(input("Podaj maksymalną cenę"))
    list_of_pets_with_specified_price = []

    for pet in outputlist:
        if pet.cena < price:
            list_of_pets_with_specified_price.append(pet)
    for pet in list_of_pets_with_specified_price:
        print(pet)
    menu()


def search_pet_by_any():
    value = input("Podaj wartosc")
    lista = []
    for pet in outputlist:
        if (pet.id == value):
            lista.append(pet)
        elif (pet.cena == value):
            lista.append(pet)
        elif (pet.kolor == value):
            lista.append(pet)
        elif (pet.gatunek == value):
            lista.append(pet)
    for pet in lista:
        print(pet)
    menu()


def search():
    global outputlist
    print("""==================Wyszukiwarka===================
1. Wyszukaj po Uuid
2. Wyszukaj po Gatunku
3. Wyszukaj po Kolorze
4. Wyszukaj po Cenie mniejszej niż
5. Wyszukaj dowolnie""")
    choise = int(input())
    if (choise == 1):
        search_pet_by_id()
    if (choise == 2):
        search_pet_by_species()
    if (choise == 3):
        search_pet_by_color()
    if (choise == 4):
        search_pet_by_price()
    if (choise == 5):
        search_pet_by_any()




